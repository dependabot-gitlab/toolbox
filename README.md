# toolbox

Automation for dependabot-gitlab projects

## jobs

### issue triage

* automated closing of stale issues for [dependabot-gitlab](https://gitlab.com/dependabot-gitlab/dependabot)

### cluster cleanup

* terminated pod cleanup from GKE cluster for [deploy](https://gitlab.com/dependabot-gitlab/deploy)

### renovate

* dependabot unsupported dependency updates for [dependabot-gitlab](https://gitlab.com/dependabot-gitlab) group
