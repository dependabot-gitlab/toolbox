# frozen_string_literal: true

require 'gitlab'

class StaleResources
  BOT_USERNAME = 'dependabot-bot'

  def self.stale?(token:, project_id:, resource_iid:, days:)
    new(token: token, project_id: project_id, resource_iid: resource_iid).days_since_last_human_update > days
  end

  def self.active?(token:, project_id:, resource_iid:, days:)
    new(token: token, project_id: project_id, resource_iid: resource_iid).days_since_last_human_update <= days
  end

  def self.core_close_note(resource)
    return unless resource[:labels].any? { |label| label.include?('dependabot-core') }

    <<~NOTE
      This issue has been marked as problem with `dependabot-core`.
      Please consult [dependabot-core issue](https://github.com/dependabot/dependabot-core/issues) tracker for possible solutions.
    NOTE
  end

  def self.bug_close_note(resource)
    return unless resource[:labels].any? { |label| label.include?('bug') }

    <<~NOTE
      If You still experience this bug on latest `dependabot-gitlab` app version, feel free to reopen this issue.
    NOTE
  end

  def initialize(token:, project_id:, resource_iid:)
    raise ArgumentError, 'An API token is needed!' if token.nil?

    @token = token
    @project_id = project_id
    @resource_iid = resource_iid
  end

  def days_since_last_human_update
    age = -1
    notes = client.send(:issue_notes, project_id, resource_iid, { order_by: 'updated_at', sort: 'desc' })
    notes.each do |note|
      next if note['author']['username'] == BOT_USERNAME

      age = age_of_timestamp(note['updated_at'])
      break
    end

    if age == -1 # If there are no comments at all, take the issue_created_date
      resource = client.send(:issue, project_id, resource_iid)
      age = age_of_timestamp(resource['created_at'])
    end

    age
  end

  private

  # Gitlab client
  #
  # @return [Gitlab::Client]
  def client
    @client ||= Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4',
      private_token: token
    )
  end

  # Age of timestamp
  #
  # @param [Date] timestamp
  # @param [Date] from
  # @return [Integer]
  def age_of_timestamp(timestamp, from: Date.today)
    (from - Date.parse(timestamp)).to_i
  end

  attr_reader :token, :project_id, :resource_iid
end
